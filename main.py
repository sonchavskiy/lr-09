def leo(n):
    if n <= 1:
        return 1
    return leo(n-1) + leo(n-2) + 1


if __name__ == '__main__':
    print(leo(8))
