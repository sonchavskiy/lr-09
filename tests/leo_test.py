from main import leo
import pytest

# взято из википедии
expected = [1, 1, 3, 5, 9, 15, 25, 41, 67, 109, 177, 287, 465, 753, 1219, 1973, 3193, 5167, 8361, 13529]


@pytest.mark.parametrize('index', list(range(20)))
def test_leo(index):
    assert leo(index) == expected[index]
